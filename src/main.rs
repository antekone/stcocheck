extern crate byteorder;
extern crate thiserror;
extern crate hex;

use std::env::args;
use std::fs::{OpenOptions, File};
use byteorder::{BE, ReadBytesExt};
use std::io::{SeekFrom, Seek, Read};
use thiserror::Error;
use std::collections::VecDeque;

#[derive(Error, Debug)]
enum AppError {
    #[error("{0}")]
    General(String),

    #[error("{0}")]
    IO(#[from] std::io::Error),
}

type AppResult<A> = std::result::Result<A, AppError>;

struct Error;
impl Error {
    fn general<A>(msg: impl AsRef<str>) -> AppResult<A> {
        Err(AppError::General(msg.as_ref().into()))
    }
}

#[derive(Debug, PartialEq)]
enum AtomKind {
    Unknown,
    Moov,
    Trak,
    Mdia,
    Hdlr,
    Minf,
    Stbl,
    Stco,
    Mdat,
}

struct Atom {
    kind: AtomKind,
    magic: u32,
    magic_str: String,
    offset: u64,
    size: u32,
}

struct State {
    stco_offsets: Vec<u64>,
    mdat: Option<(u64, u64)>, // offset, size
    moov: Option<(u64, u64)>, // offset, size
}

impl State {
    fn new() -> Self {
        Self { stco_offsets: Vec::new(), mdat: None, moov: None }
    }
}

fn read_atom(file: &mut File) -> AppResult<Atom> {
    let size = file.read_u32::<BE>()?;
    let magic = file.read_u32::<BE>()?;
    let offset = file.seek(SeekFrom::Current(0))?;

    let mut bytes = [0u8; 4];

    bytes[0] = ((magic >> 24) & 0xFF) as u8;
    bytes[1] = ((magic >> 16) & 0xFF) as u8;
    bytes[2] = ((magic >> 8) & 0xFF) as u8;
    bytes[3] = (magic & 0xFF) as u8;

    let magic_str = String::from_utf8_lossy(&bytes).into_owned();

    let atom = match magic {
        0x6D6F6F76 => Atom { kind: AtomKind::Moov, magic, magic_str, offset, size },
        0x7472616b => Atom { kind: AtomKind::Trak, magic, magic_str, offset, size },
        0x6d646961 => Atom { kind: AtomKind::Mdia, magic, magic_str, offset, size },
        0x68646c72 => Atom { kind: AtomKind::Hdlr, magic, magic_str, offset, size },
        0x6d696e66 => Atom { kind: AtomKind::Minf, magic, magic_str, offset, size },
        0x7374626c => Atom { kind: AtomKind::Stbl, magic, magic_str, offset, size },
        0x7374636f => Atom { kind: AtomKind::Stco, magic, magic_str, offset, size },
        0x6D646174 => Atom { kind: AtomKind::Mdat, magic, magic_str, offset, size },

        _ => Atom { kind: AtomKind::Unknown, magic, magic_str, offset, size },
    };

    Ok(atom)
}

fn parse(filename: &String) -> AppResult<()> {
    let mut file = OpenOptions::new().read(true).open(filename)?;
    let whole_size = file.metadata()?.len();

    let mut state = State::new();

    enter_atom(&mut state, &mut file, whole_size, 0)?;

    if state.stco_offsets.is_empty() {
        println!("No STCO atoms found in this file, aborting.");
        return Ok(());
    }

    if state.mdat.is_none() {
        println!("No MDAT atom found, aborting.");
        return Ok(());
    }

    if state.moov.is_none() {
        println!("No MOOV atom found, aborting.");
        return Ok(());
    }

    for stco_offset in &state.stco_offsets {
        process_stco(&mut file, *stco_offset, &state)?;
    }

    Ok(())
}

fn process_stco(file: &mut File, stco_offset: u64, state: &State) -> AppResult<()> {
    println!("Processing STCO: 0x{:08x}", stco_offset);

    file.seek(SeekFrom::Start(stco_offset))?;

    let _ = file.read_u32::<BE>()?;
    let items = file.read_u32::<BE>()?;

    let (mdat_offset, mdat_size) = state.mdat.unwrap();
    let (moov_offset, moov_size) = state.moov.unwrap();

    println!("MDAT found at 0x{:08x}-0x{:08x}", mdat_offset, mdat_offset + mdat_size - 1);
    println!("MOOV found at 0x{:08x}-0x{:08x}", moov_offset, moov_offset + moov_size - 1);
    println!("STCO item count: {}", items);

    for i in 0..items {
        let cur = file.seek(SeekFrom::Current(0))?;
        let data_offset = file.read_u32::<BE>()?;

        file.seek(SeekFrom::Start(data_offset as u64))?;
        let mut sample = [0u8; 16];
        let _ = file.read_exact(&mut sample)?;
        let hex_str = hex::encode(sample);

        file.seek(SeekFrom::Start(cur + 4))?;

        let warn = if (data_offset as u64) < mdat_offset {
            "!!! Points before MDAT"
        } else if (data_offset as u64) >= (mdat_offset + mdat_size) {
            "!!! Points after MDAT"
        } else { "(inside MDAT)" };

        println!("{:5} @ [0x{:08x}] = 0x{:08x} --> {} {}", i, cur, data_offset, hex_str, warn);
    }

    Ok(())
}

fn enter_atom(state: &mut State, file: &mut File, max_offset: u64, indent: usize) -> AppResult<()> {
    let mut indent_str = String::new();
    for i in 0..indent {
        indent_str += "    ";
    }

    loop {
        let offset = file.seek(SeekFrom::Current(0))?;
        if offset >= max_offset { break; }

        let atom = read_atom(file)?;
        if atom.size == 0 { break; }

        let enter = match atom.kind {
            AtomKind::Moov => true,
            AtomKind::Trak => true,
            AtomKind::Mdia => true,
            AtomKind::Minf => true,
            AtomKind::Stbl => true,
            _ => false
        };

        // println!("{}Encountered offset 0x{:08x} -- atom {} (0x{:08x}), size=0x{:08x}", indent_str, offset, atom.magic_str, atom.magic, atom.size);

        if atom.kind == AtomKind::Stco {
            state.stco_offsets.push(atom.offset);
        } else if atom.kind == AtomKind::Mdat {
            state.mdat = Some((atom.offset, atom.size as u64 - 8));
        } else if atom.kind == AtomKind::Moov {
            state.moov = Some((atom.offset, atom.size as u64 - 8));
        }

        let next_atom = offset + atom.size as u64;
        if enter {
            enter_atom(state, file, offset + atom.size as u64, indent + 1)?;
        }

        file.seek(SeekFrom::Start(next_atom))?;
    }

    Ok(())
}

fn main() {
    let args = args().collect::<Vec<String>>();
    parse(&args[1]);
}
